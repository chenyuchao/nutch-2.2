/*
Navicat MySQL Data Transfer

Source Server         : host129
Source Server Version : 50621
Source Host           : 192.168.200.129:3306
Source Database       : nutch

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-04-01 16:17:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for webpage
-- ----------------------------
DROP TABLE IF EXISTS `webpage`;
CREATE TABLE `webpage` (
  `id` varchar(190) NOT NULL,
  `headers` blob,
  `商品名称` varchar(500) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `markers` blob,
  `parseStatus` blob,
  `modifiedTime` bigint(20) DEFAULT NULL,
  `prevModifiedTime` bigint(20) DEFAULT NULL,
  `score` float DEFAULT NULL,
  `typ` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `batchId` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `baseUrl` varchar(767) DEFAULT NULL,
  `content` longblob,
  `title` varchar(2048) DEFAULT NULL,
  `商品价格` varchar(10) DEFAULT NULL,
  `品目树节点` varchar(1000) DEFAULT NULL,
  `规格参数` text,
  `商品介绍` text,
  `HASH值` varchar(50) DEFAULT NULL,
  `是否京东自营` varchar(10) DEFAULT NULL,
  `reprUrl` varchar(767) DEFAULT NULL,
  `fetchInterval` int(11) DEFAULT NULL,
  `prevFetchTime` bigint(20) DEFAULT NULL,
  `inlinks` mediumblob,
  `prevSignature` blob,
  `outlinks` mediumblob,
  `fetchTime` bigint(20) DEFAULT NULL,
  `retriesSinceFetch` int(11) DEFAULT NULL,
  `protocolStatus` blob,
  `signature` blob,
  `metadata` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPRESSED;
