/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.apache.nutch.parse;

public class Parse {

	private String text;
	private String title;
	private String productname;
	private String price;
	private String navigation;
	private String parameters;
	private String introduction;
	private String hashvalue;
	private String isself;

	private Outlink[] outlinks;
	private org.apache.nutch.storage.ParseStatus parseStatus;

	public Parse() {
	}

	public Parse(String text, String title, Outlink[] outlinks,
			org.apache.nutch.storage.ParseStatus parseStatus) {
		this.text = text;
		this.title = title;
		this.outlinks = outlinks;
		this.parseStatus = parseStatus;
	}

	public Parse(String title, String productname, String price,
			String navigation, String parameters, String introduction,
			String hashvalue, String isself, Outlink[] outlinks,
			org.apache.nutch.storage.ParseStatus parseStatus) {
		this.text = "";
		this.title = title;
		this.productname = productname;
		this.price = price;
		this.navigation = navigation;
		this.parameters = parameters;
		this.introduction = introduction;
		this.hashvalue = hashvalue;
		this.isself = isself;
		this.outlinks = outlinks;
		this.parseStatus = parseStatus;
	}

	// public String getText() {
	// return text;
	// }

	public String getTitle() {
		return title;
	}

	public String getProductName() {
		return productname;
	}

	public String getPrice() {
		return price;
	}

	public String getNavigation() {
		return navigation;
	}

	public String getParameters() {
		return parameters;
	}

	public String getIntroduction() {
		return introduction;
	}

	public String getHashvalue() {
		return hashvalue;
	}

	public String getIsself() {
		return isself;
	}

	public Outlink[] getOutlinks() {
		return outlinks;
	}

	public org.apache.nutch.storage.ParseStatus getParseStatus() {
		return parseStatus;
	}

	// public void setText(String text) {
	// this.text = text;
	// }

	public void setTitle(String title) {
		this.title = title;
	}

	public void setProductName(String productname) {
		this.productname = productname;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setNavigation(String navigation) {
		this.navigation = navigation;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public void setHashvalue(String hashvalue) {
		this.hashvalue = hashvalue;
	}

	public void setIsself(String isself) {
		this.isself = isself;
	}

	public void setOutlinks(Outlink[] outlinks) {
		this.outlinks = outlinks;
	}

	public void setParseStatus(org.apache.nutch.storage.ParseStatus parseStatus) {
		this.parseStatus = parseStatus;
	}
}
