/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.nutch.parse.html;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.avro.util.Utf8;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.html.dom.HTMLDocumentImpl;
import org.apache.nutch.metadata.Metadata;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.parse.HTMLMetaTags;
import org.apache.nutch.parse.ParseFilters;
import org.apache.nutch.parse.Outlink;
import org.apache.nutch.parse.Parse;
import org.apache.nutch.parse.ParseStatusCodes;
import org.apache.nutch.parse.ParseStatusUtils;
import org.apache.nutch.parse.Parser;
import org.apache.nutch.storage.ParseStatus;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.Bytes;
import org.apache.nutch.util.EncodingDetector;
import org.apache.nutch.util.NutchConfiguration;
import org.apache.nutch.util.TableUtil;
import org.cyberneko.html.parsers.DOMFragmentParser;
import org.cyberneko.html.parsers.DOMParser;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import jcifs.smb.*;

public class HtmlParser implements Parser {
	public static final Logger LOG = LoggerFactory
			.getLogger("org.apache.nutch.parse.html");

	// I used 1000 bytes at first, but found that some documents have
	// meta tag well past the first 1000 bytes.
	// (e.g. http://cn.promo.yahoo.com/customcare/music.html)
	private static final int CHUNK_SIZE = 2000;

	// NUTCH-1006 Meta equiv with single quotes not accepted
	private static Pattern metaPattern = Pattern.compile(
			"<meta\\s+([^>]*http-equiv=(\"|')?content-type(\"|')?[^>]*)>",
			Pattern.CASE_INSENSITIVE);
	private static Pattern charsetPattern = Pattern.compile(
			"charset=\\s*([a-z][_\\-0-9a-z]*)", Pattern.CASE_INSENSITIVE);

	private static Collection<WebPage.Field> FIELDS = new HashSet<WebPage.Field>();

	static {
		FIELDS.add(WebPage.Field.BASE_URL);
	}

	private String parserImpl;

	/**
	 * Given a <code>byte[]</code> representing an html file of an
	 * <em>unknown</em> encoding, read out 'charset' parameter in the meta tag
	 * from the first <code>CHUNK_SIZE</code> bytes. If there's no meta tag for
	 * Content-Type or no charset is specified, <code>null</code> is returned. <br />
	 * FIXME: non-byte oriented character encodings (UTF-16, UTF-32) can't be
	 * handled with this. We need to do something similar to what's done by
	 * mozilla
	 * (http://lxr.mozilla.org/seamonkey/source/parser/htmlparser/src/nsParser
	 * .cpp#1993). See also http://www.w3.org/TR/REC-xml/#sec-guessing <br />
	 *
	 * @param content
	 *            <code>byte[]</code> representation of an html file
	 */

	private static String sniffCharacterEncoding(byte[] content) {
		int length = content.length < CHUNK_SIZE ? content.length : CHUNK_SIZE;

		// We don't care about non-ASCII parts so that it's sufficient
		// to just inflate each byte to a 16-bit value by padding.
		// For instance, the sequence {0x41, 0x82, 0xb7} will be turned into
		// {U+0041, U+0082, U+00B7}.
		String str = "";
		try {
			str = new String(content, 0, length, Charset.forName("ASCII")
					.toString());
		} catch (UnsupportedEncodingException e) {
			// code should never come here, but just in case...
			return null;
		}

		Matcher metaMatcher = metaPattern.matcher(str);
		String encoding = null;
		if (metaMatcher.find()) {
			Matcher charsetMatcher = charsetPattern.matcher(metaMatcher
					.group(1));
			if (charsetMatcher.find())
				encoding = new String(charsetMatcher.group(1));
		}

		return encoding;
	}

	private String defaultCharEncoding;

	private Configuration conf;

	private DOMContentUtils utils;

	private ParseFilters htmlParseFilters;

	private String cachingPolicy;

	public Parse getParse(String url, WebPage page) {
		HTMLMetaTags metaTags = new HTMLMetaTags();

		String baseUrl = TableUtil.toString(page.getBaseUrl());
		URL base;
		try {
			base = new URL(baseUrl);
		} catch (MalformedURLException e) {
			return ParseStatusUtils.getEmptyParse(e, getConf());
		}

		String title = "";
		String productname = "";
		String price = "";
		String navigation = "";
		String parameters = "";
		String introduction = "";
		String hashvalue = "";
		String isself = "";
		Outlink[] outlinks = new Outlink[0];
		Metadata metadata = new Metadata();

		// parse the content
		DocumentFragment root;

		try {
			byte[] contentInOctets = page.getContent().array();
			InputSource input = new InputSource(new ByteArrayInputStream(
					contentInOctets));
			EncodingDetector detector = new EncodingDetector(conf);
			detector.autoDetectClues(page, true);
			detector.addClue(sniffCharacterEncoding(contentInOctets), "sniffed");
			String encoding = detector.guessEncoding(page, defaultCharEncoding);

			metadata.set(Metadata.ORIGINAL_CHAR_ENCODING, encoding);
			metadata.set(Metadata.CHAR_ENCODING_FOR_CONVERSION, encoding);

			input.setEncoding(encoding);

			if (LOG.isTraceEnabled()) {
				LOG.trace("Parsing...");
			}
			root = parse(input);
		} catch (IOException e) {
			LOG.error("Failed with the following IOException: ", e);
			return ParseStatusUtils.getEmptyParse(e, getConf());
		} catch (DOMException e) {
			LOG.error("Failed with the following DOMException: ", e);
			return ParseStatusUtils.getEmptyParse(e, getConf());
		} catch (SAXException e) {
			LOG.error("Failed with the following SAXException: ", e);
			return ParseStatusUtils.getEmptyParse(e, getConf());
		} catch (Exception e) {
			LOG.error("Failed with the following Exception: ", e);
			return ParseStatusUtils.getEmptyParse(e, getConf());
		}

		// get meta directives
		HTMLMetaProcessor.getMetaTags(metaTags, root, base);
		if (LOG.isTraceEnabled()) {
			LOG.trace("Meta tags for " + base + ": " + metaTags.toString());
		}

		if (!metaTags.getNoIndex()) {

			// TODO URL filter
			byte[] contentInOctets = page.getContent().array();
			InputSource input = new InputSource(new ByteArrayInputStream(
					contentInOctets));

			StringBuilder sb = new StringBuilder();
			if (LOG.isTraceEnabled()) {
				LOG.trace("Getting title...");
			}
			utils.getTitle(sb, root); // extract title
			title = sb.toString();

			Map<String, String> colMeta = this.getColumns(input);
			productname = colMeta.get("productname");
			parameters = colMeta.get("parameters");
			price = colMeta.get("price");
			navigation = colMeta.get("navigation");
			introduction = colMeta.get("introduction");
			hashvalue = colMeta.get("hashvalue");
			isself = colMeta.get("isself");
		}

		if (!metaTags.getNoFollow()) { // okay to follow links
			ArrayList<Outlink> l = new ArrayList<Outlink>(); // extract outlinks
			URL baseTag = utils.getBase(root);
			if (LOG.isTraceEnabled()) {
				LOG.trace("Getting links...");
			}
			utils.getOutlinks(baseTag != null ? baseTag : base, l, root);
			outlinks = l.toArray(new Outlink[l.size()]);
			if (LOG.isTraceEnabled()) {
				LOG.trace("found " + outlinks.length + " outlinks in " + url);
			}
		}

		ParseStatus status = new ParseStatus();
		status.setMajorCode(ParseStatusCodes.SUCCESS);
		if (metaTags.getRefresh()) {
			status.setMinorCode(ParseStatusCodes.SUCCESS_REDIRECT);
			status.addToArgs(new Utf8(metaTags.getRefreshHref().toString()));
			status.addToArgs(new Utf8(Integer.toString(metaTags
					.getRefreshTime())));
		}

		Parse parse = new Parse(title, productname, price, navigation,
				parameters, introduction, hashvalue, isself, outlinks, status);
		parse = htmlParseFilters.filter(url, page, parse, metaTags, root);

		if (metaTags.getNoCache()) { // not okay to cache
			page.putToMetadata(new Utf8(Nutch.CACHING_FORBIDDEN_KEY),
					ByteBuffer.wrap(Bytes.toBytes(cachingPolicy)));
		}

		return parse;
	}

	private DocumentFragment parse(InputSource input) throws Exception {
		if (parserImpl.equalsIgnoreCase("tagsoup"))
			return parseTagSoup(input);
		else
			return parseNeko(input);
	}

	private DocumentFragment parseTagSoup(InputSource input) throws Exception {
		HTMLDocumentImpl doc = new HTMLDocumentImpl();
		DocumentFragment frag = doc.createDocumentFragment();
		DOMBuilder builder = new DOMBuilder(doc, frag);
		org.ccil.cowan.tagsoup.Parser reader = new org.ccil.cowan.tagsoup.Parser();
		reader.setContentHandler(builder);
		reader.setFeature(org.ccil.cowan.tagsoup.Parser.ignoreBogonsFeature,
				true);
		reader.setFeature(org.ccil.cowan.tagsoup.Parser.bogonsEmptyFeature,
				false);
		reader.setProperty("http://xml.org/sax/properties/lexical-handler",
				builder);
		reader.parse(input);
		return frag;
	}

	private DocumentFragment parseNeko(InputSource input) throws Exception {
		DOMFragmentParser parser = new DOMFragmentParser();
		try {
			parser.setFeature(
					"http://cyberneko.org/html/features/augmentations", true);
			parser.setProperty(
					"http://cyberneko.org/html/properties/default-encoding",
					defaultCharEncoding);
			parser.setFeature(
					"http://cyberneko.org/html/features/scanner/ignore-specified-charset",
					true);
			parser.setFeature(
					"http://cyberneko.org/html/features/balance-tags/ignore-outside-content",
					false);
			parser.setFeature(
					"http://cyberneko.org/html/features/balance-tags/document-fragment",
					true);
			parser.setFeature(
					"http://cyberneko.org/html/features/report-errors",
					LOG.isTraceEnabled());
		} catch (SAXException e) {
		}
		// convert Document to DocumentFragment
		HTMLDocumentImpl doc = new HTMLDocumentImpl();
		doc.setErrorChecking(false);
		DocumentFragment res = doc.createDocumentFragment();
		DocumentFragment frag = doc.createDocumentFragment();
		parser.parse(input, frag);
		res.appendChild(frag);

		try {
			while (true) {
				frag = doc.createDocumentFragment();
				parser.parse(input, frag);
				if (!frag.hasChildNodes())
					break;
				if (LOG.isInfoEnabled()) {
					LOG.info(" - new frag, " + frag.getChildNodes().getLength()
							+ " nodes.");
				}
				res.appendChild(frag);
			}
		} catch (Exception x) {
			LOG.error("Failed with the following Exception: ", x);
		}
		;
		return res;
	}

	public void setConf(Configuration conf) {
		this.conf = conf;
		this.htmlParseFilters = new ParseFilters(getConf());
		this.parserImpl = getConf().get("parser.html.impl", "neko");
		this.defaultCharEncoding = getConf().get(
				"parser.character.encoding.default", "windows-1252");
		this.utils = new DOMContentUtils(conf);
		this.cachingPolicy = getConf().get("parser.caching.forbidden.policy",
				Nutch.CACHING_FORBIDDEN_CONTENT);
	}

	public Configuration getConf() {
		return this.conf;
	}

	@Override
	public Collection<WebPage.Field> getFields() {
		return FIELDS;
	}

	public static void main(String[] args) throws Exception {
		// LOG.setLevel(Level.FINE);
		String name = args[0];
		String url = "file:" + name;
		File file = new File(name);
		byte[] bytes = new byte[(int) file.length()];
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		in.readFully(bytes);
		Configuration conf = NutchConfiguration.create();
		HtmlParser parser = new HtmlParser();
		parser.setConf(conf);
		WebPage page = new WebPage();
		page.setBaseUrl(new Utf8(url));
		page.setContent(ByteBuffer.wrap(bytes));
		page.setContentType(new Utf8("text/html"));
		Parse parse = parser.getParse(url, page);
		System.out.println("title: " + parse.getTitle());
		System.out.println("productname: " + parse.getProductName());
		// System.out.println("text: " + parse.getText());
		System.out.println("outlinks: " + Arrays.toString(parse.getOutlinks()));

	}

	// extract parameters
	public String getParameters(org.w3c.dom.Document doc) throws Exception {

		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();
		Node currentNode;
		NodeList n = (NodeList) xpath.evaluate("//TABLE[@class='Ptable']//TR",
				doc, XPathConstants.NODESET);
		String rsJSON = "{";
		for (int i = 0; i < n.getLength(); i++) {
			// 目前京东只有两项
			currentNode = n.item(i);
			if ("TH".equalsIgnoreCase(currentNode.getFirstChild().getNodeName())) {
				if (i != 0) {
					rsJSON = rsJSON.substring(0, rsJSON.length() - 1) + "},";
				}
				rsJSON += "\""
						+ ToJSONString(currentNode.getFirstChild()
								.getTextContent()) + "\":{";
			} else {
				if ("TD".equalsIgnoreCase(currentNode.getFirstChild()
						.getNodeName())) {
					rsJSON += "\""
							+ ToJSONString(currentNode.getFirstChild()
									.getTextContent()) + "\":";
					rsJSON += "\""
							+ ToJSONString(currentNode.getLastChild()
									.getTextContent()) + "\",";
				}
			}
		}

		// 去掉最后一个逗号
		rsJSON = rsJSON.substring(0, rsJSON.length() - 1) + "}}";
		// 特殊情况下
		if (rsJSON.length() < 5) {
			Node node = (Node) xpath.evaluate("//DIV[@id='product-detail-2']",
					doc, XPathConstants.NODE);
			rsJSON = Pattern.compile("\\s").matcher(node.getTextContent())
					.replaceAll("");
		}
		return rsJSON;
	}

	// extract pictures
	public void getPictures(Document doc) {
		try {
			XPathFactory xpathFactory = XPathFactory.newInstance();
			XPath xpath = xpathFactory.newXPath();

			NodeList nlst = (NodeList) xpath.evaluate(
					"//DIV[@class='spec-items']//IMG", doc,
					XPathConstants.NODESET);

			int psize = nlst.getLength();
			if (psize <= 0)
				return;

			// 获得商品编号
			String NO = (String) xpath.evaluate(
					"//DIV[@class='fl']//SPAN[last()]", doc,
					XPathConstants.STRING);

			for (int i = 0; i < psize; i++) {
				String src = nlst.item(i).getAttributes().getNamedItem("src")
						.getNodeValue();
				savePictures(picUrlHandler(src), NO, i + "");
			}
		} catch (Exception e) {
			// don't care
		}
	}

	public void getPicturesByURL(String src, String path) {
		try {
			URL url = new URL(src);
			// 打开链接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// 设置请求方式为"GET"
			conn.setRequestMethod("GET");
			// 超时响应时间为5秒
			conn.setConnectTimeout(5 * 1000);
			// 通过输入流获取图片数据
			InputStream inStream = conn.getInputStream();
			// 得到图片的二进制数据，以二进制封装得到数据，具有通用性
			byte[] data = readInputStream(inStream);
			// new一个文件对象用来保存图片，默认保存当前工程根目录
			File imageFile = new File(path);

			// 创建输出流
			FileOutputStream outStream = new FileOutputStream(imageFile);
			// 写入数据
			outStream.write(data);
			// 关闭输出流
			outStream.close();
		} catch (Exception e) {
			// don't care
		}
	}

	public void savePictures(String src, String NO, String index) {
		InputStream in = null;
		OutputStream out = null;
		try {
			URL url = new URL(src);
			// 打开链接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// 设置请求方式为"GET"
			conn.setRequestMethod("GET");
			// 超时响应时间为5秒
			conn.setConnectTimeout(5 * 1000);
			// 通过输入流获取图片数据
			InputStream inStream = conn.getInputStream();

			String dir = "smb://administrator:Epointkf4b@192.168.200.129/share/JD/"
					+ NO;
			SmbFile remoteDir = new SmbFile(dir);

			if (!remoteDir.exists()) {
				remoteDir.mkdir();
			}

			SmbFile remoteFile = new SmbFile(dir + "/" + NO + "_" + index
					+ ".jpg");
			remoteFile.connect();

			in = new BufferedInputStream(inStream);
			out = new BufferedOutputStream(new SmbFileOutputStream(remoteFile));

			byte[] buffer = new byte[4096];
			int len = 0; // 读取长度
			while ((len = in.read(buffer, 0, buffer.length)) != -1) {
				out.write(buffer, 0, len);
			}
			out.flush(); // 刷新缓冲的输出流

		} catch (Exception e) {
			// don't care
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (Exception e) {
			}
		}
	}

	public byte[] readInputStream(InputStream inStream) throws Exception {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		// 创建一个Buffer字符串
		byte[] buffer = new byte[1024];
		// 每次读取的字符串长度，如果为-1，代表全部读取完毕
		int len = 0;
		// 使用一个输入流从buffer里把数据读取出来
		while ((len = inStream.read(buffer)) != -1) {
			// 用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
			outStream.write(buffer, 0, len);
		}
		// 关闭输入流
		inStream.close();
		// 把outStream里的数据写入内存
		return outStream.toByteArray();
	}

	// 根据缩略图地址获取n1 大小的图片地址
	public static String picUrlHandler(String picsrc) {
		Pattern p = Pattern.compile("\\.360buyimg.com/n\\d+");
		picsrc = p.matcher(picsrc).replaceAll(".360buyimg.com/n1");
		return picsrc;
	}

	// 去除JSON中的特殊字符
	public String ToJSONString(String text) {
		char[] charArray = text.toCharArray();
		List<String> output = new ArrayList<String>();
		for (char c : charArray) {
			if (((int) c) == 8) // Backspace
				output.add("\\b");
			else if (((int) c) == 9) // Horizontal tab
				output.add("\\t");
			else if (((int) c) == 10) // Newline
				output.add("\\n");
			else if (((int) c) == 12) // Formfeed
				output.add("\\f");
			else if (((int) c) == 13) // Carriage return
				output.add("\\n");
			else if (((int) c) == 34) // Double-quotes (")
				output.add("\\" + c + "");
			else if (((int) c) == 47) // Solidus (/)
				output.add("\\" + c + "");
			else if (((int) c) == 92) // Reverse solidus (\)
				output.add("\\" + c + "");
			else if (((int) c) > 31)
				output.add(c + "");
		}

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < output.size(); i++) {
			sb.append(output.get(i));
		}
		return new String(sb);
	}

	public Map<String, String> getColumns(InputSource input) {
		Map<String, String> results = new HashMap<String, String>();
		try {
			DOMParser parser = new DOMParser();
			parser.parse(input);
			Document doc = parser.getDocument();
			XPathFactory xpathFactory = XPathFactory.newInstance();
			XPath xpath = xpathFactory.newXPath();
			Node node;
			// filter unwanted characters
			Pattern p = Pattern.compile("\\s");

			Map<String, String> rules = new HashMap<String, String>();
			rules.put("productname", "//DIV[@id='name']");
			rules.put("navigation", "//DIV[@id='root-nav']");
			rules.put("introduction", "//UL[@id='parameter2']");

			for (Entry<String, String> rule : rules.entrySet()) {
				node = (Node) xpath.evaluate(rule.getValue(), doc,
						XPathConstants.NODE);
				if (node != null) {
					results.put(rule.getKey(), p.matcher(node.getTextContent())
							.replaceAll(""));
				} else {
					results.put(rule.getKey(), "parse failed");
				}
			}

			// is self 单独判断效率好一点
			node = (Node) xpath.evaluate("//DIV[@class='seller-infor']", doc,
					XPathConstants.NODE);
			if (node != null) {
				String tmp = p.matcher(node.getTextContent()).replaceAll("");
				if ("京东自营".equals(tmp)) {
					tmp = "1";
				} else {
					tmp = "0";
				}
				results.put("isself", tmp);
			} else {
				results.put("isself", "-1");
			}

			// extract parameters
			try {
				results.put("parameters", getParameters(doc));
			} catch (Exception e) {
				results.put("parameters", "parse failed");
			}

			// TODO extract price
			String itemNum = (String) xpath.evaluate(
					"//DIV[@class='fl']//SPAN[last()]", doc,
					XPathConstants.STRING);
			results.put("price", getProductPrice(itemNum));

			// TODO extract hash value
			results.put("hashvalue", "sha-1");

			// 这个的error 不会抛出
			this.getPictures(doc);

		} catch (Exception e) {
			// don't care
			results.clear();
			results.put("price", "parse failed");
			results.put("productname", "parse failed");
			results.put("navigation", "parse failed");
			results.put("introduction", "parse failed");
			results.put("isself", "-1");
			results.put("hashvalue", "parse failed");
		}

		return results;
	}

	public String getProductPrice(String productNum) {
		String price = "-1";
		try {
			String _url = "http://p.3.cn/prices/mgets?skuIds=J_" + productNum;
			URL url = new URL(_url);
			// 打开链接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(5 * 1000);
			InputStream inStream = conn.getInputStream();
			byte[] data = readInputStream(inStream);
			String tmp = new String(data, "UTF-8");
			String exp = "\"p\":\"([0-9.]+)";
			Pattern p = Pattern.compile(exp);
			Matcher m = p.matcher(tmp);
			if (m.find()) {
				price = m.group(1);
			}
		} catch (Exception e) {
			// don't care
		}
		return price;
	}
}
